/* Joshua Beck

This program creates a buffer of numbers which represents a pool of resources.
Two seperate threads are created.  One is a producer() thread and the other is a 
consumer() thread.  

The producer() thread gets a number and places it into the buffer.  Whenever the buffer
is filled, the producer() thread will sleep.  Anytime the buffer has any values in it, the 
producer() thread attempts to wake the consumer() thread.

On the other hand, the consumer() thread takes a number from the buffer.  Whenever the buffer is empty,
the consumer() thread will sleep.  Similarily, whenever the buffer is not full, the consumer()
thread attempts to wake the producer() thread.

This structure is created so that at any point, there is not a shortage or overage of components
for the producer or consumer.  However, as switching between threads can be finicky on some CPU's, 
the utilization of sigwait and pthread_kill is used so that we can asynchronously synchronize the
processes.  Furthermore, the buffer helps to safeguard any disparity between the rate of the producer
and consumer.

*/

#include <stdio.h>
#include <pthread.h>
#include  <signal.h>
#include <stdio.h>
#include<unistd.h>

int maxValue = 100;

// Shared Circular Buffer
struct CIRCULAR_BUFFER
{

  int count;			// Number of items in the buffer
  int lower;			// Next slot to read in the buffer
  int upper;			// Next slot to write in the buffer
  int buffer[100];
};

//Create an instance of the circular buffer
struct CIRCULAR_BUFFER buffer;

//Add a new value to the buffer and increment the upper bound index
void
put (int i)
{


  buffer.buffer[buffer.upper] = i;

  ++buffer.count;
  ++buffer.upper;
  buffer.upper = buffer.upper % maxValue;
}

//This method will remove the next value from the buffer and return it
int
take ()
{

  //Read the next value into a temp variable
  int nextValue = buffer.buffer[buffer.lower];
 
  buffer.buffer[buffer.lower] = -1;
  --buffer.count;
  int newLower;
  if (buffer.upper - buffer.count < 0){
  	buffer.lower = (buffer.upper - buffer.count + maxValue) % maxValue;
  } else {
  	buffer.lower = (buffer.upper - buffer.count) % maxValue;
  }
  

  return nextValue;
}

int nextValue = 0;
int
getNewValue ()
{
  return nextValue++;
}

//******************************//
sigset_t sigSet;

pthread_t cID;
pthread_t dID;

void *
producer (void *abc)
{

  int i = 0;
  int nSig;
  printf ("Producer thread starting\n");
  while (1==1)
    {

      if (buffer.count == 1)
	{
	  //Awake the consumer thread
	  printf ("Attempting to start the consumer thread...\n");

	  //If the parent thread was consumer, send the kill signal; otherwise, pause this thread and move action to consumer
	  pthread_kill (cID, SIGUSR1);
	  sigwait (&sigSet, &nSig);
	
	}

      if (buffer.count < maxValue)
	{
	  printf("Put a new value in buffer\n");
	  put (getNewValue ());
	}
      else
	{
	  printf ("The buffer is full... sleep producer thread\n");
	  //We have maxed out the buffer-go to sleep
 	  pthread_kill (cID, SIGUSR1);
	  sigwait (&sigSet, &nSig);
 	  printf("Awaking the producer thread...\n");
	}

    }
}

void *
consumer (void *abc)
{
  printf ("Consumer thread starting\n");

  //Continue loop indefinitely
  int nSig;
  while (1==1)
    {
      
      if (buffer.count == maxValue - 1)
	{
	  printf ("Attempt to start the producing thread...\n");

	  //If the parent thread was consumer, send the kill signal; otherwise, pause this thread and move action to consumer
	  pthread_kill (dID, SIGUSR1);
	  sigwait (&sigSet, &nSig);
	

	}
      if (buffer.count > 0)
	{
	  int value = take ();
	  printf ("Remove the # %d from buffer\n", value);
	}
      else
	{
	  //There are no values remaining in the buffer... sleep consumer thread
	  // printf("No values remaining in the buffer... sleep consumer thread\n");
	  pthread_kill (dID, SIGUSR1);
	  sigwait (&sigSet, &nSig);
	  printf("Awaking the consumer thread...\n");

	}

    }
  _exit (1);


}

int
main ()
{
  printf ("Creating the threads and setting the signals...");
  sigemptyset (&sigSet);
  sigaddset (&sigSet, SIGUSR1);
  sigaddset (&sigSet, SIGSEGV);

  pthread_sigmask (SIG_BLOCK, &sigSet, NULL);

  pthread_create (&dID, NULL, producer, NULL);
  pthread_create (&cID, NULL, consumer, NULL);

  //After the threads are created and utilized, join them to the main thread
  pthread_join (dID, NULL);
  pthread_join (cID, NULL);

}